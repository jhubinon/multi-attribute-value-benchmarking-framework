#/usr/bin/env python
import pandas as pd
import numpy as np

def get_data(name):
	df = pd.read_excel(name)
	G = df.columns.tolist()
	print(G)
	df = np.array(df)
	#Evaluations of the alternatives
	E = []
	#Names of the alternatives
	A = []
	#fill A and E with df
	for elem in df:
		A.append(elem[0])
		E.append([elem[i] for i in range(1,len(elem))])
		#R.append(elem[8])
	#The names of the criteria
	return E,A,G