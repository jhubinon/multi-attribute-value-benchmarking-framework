**Long-term Multi-criteria strategy of improvement**

This project aims at building a software (MCDMBM.exe in the download files) related to a paper named 
"Long-term Multi-criteria strategy of improvement" written and developed by Hubinont Jean-Philippe and 
De Smet Yves in [...Paper REF...].

We also share the Python files that were used to build this software using the PySimplGUI package 
(https://pysimplegui.readthedocs.io/en/latest/)

The python code could be much improved by a computer scientist but it aims at allowing any novice with
computer science to enjoy the framework we propose. All the details of the methodology is provided in
[...Paper REF...]

## How does MAVBM.exe works?
1. Download the executable and double click on it to launch it.
2. Browse an .xlsx file that contains the performance table.
	A file named "ARWU_LXL_FCPROMII.xlsx" is provided in Downloads so that you design your input file correctly:
	The first row must be the headings of the table. The first column contains all the names of the different
	observed alternatives used to build a strategy of improvement. 
3. Browse a folder where the resulting tables will be saved.
4. A new tab appears: Data. It shows the performance table and allows you to select which alternative must be 
	improved (don't forget to click on submit); to select the direction of optimization of each criterion (min/max) and 
	to generate partial values for the MAVT model. 
	These values should be elicitated carefully. The bisection method is implemented for 5 reference 
	points (see:Edwards, Ward, Ralph F. Miles, and Detlof Von Winterfeldt. "Advances in decision analysis." Cambridge, 
	New York (2007). 
5. A new tab appears: Risk. It relates to the penalty weights related to improving while risking to face a bottleneck 
	or related to improving and requiring an operational change. See the article to understand what each risk includes
	and how it is modeled. This tab allows you to refine your preference regarding the latter two risks. If there is no 
	preference, select the last option. Click on Submit.
6. A new tab appears: Paths. Click on generate paths. A minimum and maximum absolute performance in (the initial performance
	scales) will be required. The weight attributed to each criterion is also required. For a MAVT model, it a acts as a
	scaling factor. Be sure to use an appropriate elicitation method for the weights.
    Depending on the amount of alternatives, on the amount of criteria, the scale used and the amount of improvement 
	that the improving alternative can reach, it can take several seconds or several minutes. A progress status shows 
	the percentage of the solutions found.
7. The different paths of improvement are provided in the tab 'PATHS'. Each step is represented as follows:
	(reference alternative - criterion to improve +(the amount of rank earned)[!B!] with the last component [!B!] being
	optional and alerts the decision maker that this step risks facing a bottleneck. The last three columns are:
	a. The amount of ranks earned while facing a risk of bottleneck
	b. The amount of ranks earned with an operational change
	c. The total amount of ranks earned.
	Select a path and click on 'visualize' to see a bar chart of representing the improvement path. Its title indicates
	the centroide of all the pair of penalty weights that allow this path to be optimal. A percentage of all the pairs
	tested provides an information of 'robustness' regarding the optimality of the latter path.
8. The last tab allows the analyst or the decision maker to understand which paths are optimal for which values of
	the penalty weights. It is an important step. For instance, if the decision maker believes that a path should be
	more penalized for a risk of bottleneck than an operational change, a path that is optimal for any w_bottleneck >= w_op
	if probably more fit than a path that is optimal for any w_bottleneck < w_op. The figures drawn are useful to have
	a fast glimpse of the optimality of each path in the weight's values space.
9.	It is possible to save configurations to load in another session the same values for the partial values, worst and
	best performances, scaling factors, etc. There is a menu named 'File'. Just select 'Save' or 'Load'.