import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
def overlapped_bar(df, alt, centroide, width=0.9, alpha=.5,
                   title='Path of improvement', xlabel='criteria', ylabel='Partial value increase', **plot_kwargs):
    """Like a stacked bar chart except bars on top of each other with transparency"""
    xlabel = xlabel or any(df.index)
    colors = {}
    def get_cmap(n, name='hsv'):
        return plt.cm.get_cmap(name, n)

    c = get_cmap(len(alt))
    i = 0
    for ref in alt:
        colors[ref] = c(i)
        i+=1

    fig, ax = plt.subplots()
    N = len(df)
    M = len(df.columns)
    indices = np.arange(N)
    starting = 0
    for i, label in zip(range(M), df.columns):
        kwargs = plot_kwargs
        kwargs.update({'label': label})
        rect = ax.bar(indices, df[label], bottom=starting, width=width, color = colors[alt[i]], alpha=alpha if i else 1, **kwargs)
        plt.xticks(indices + 0 * width,
                   ['{}'.format(idx) for idx in df.index.values])
        starting+= max(df[label])
        #height = rect.get_height()
        plt.text(list(df[label]).index(max(df[label])),starting, alt[i][:6])
        #ax.text(rect.get_x() + rect.get_width()/2., height-0.15,
                #'%d' % int(height),
                #ha='center', va='bottom')
    plt.legend()
    plt.title(title+ ' | w_centr. = ' + '|' + centroide)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()
    return fig

def plot_path(p,alt,crit,centroide):
    imp = []
    for i in range(1,len(p)):
        imp.append([(p[i][k]-p[i-1][k])/1000 for k in range(len(p[0]))])
    print(np.matrix(imp))
    print(alt)
    print(crit)
    df = pd.DataFrame(np.matrix(imp).T,columns=['step'+str(j) for j in range(1,len(p))],
                  index=crit)
    print(df.index.values)
    overlapped_bar(df, alt, centroide)

def get_figure(p, alt, crit,centroide): 
    fig = plot_path(p, alt, crit,centroide)

if __name__ == '__main__':
    p = [(187, 1000, 97, 47, 477, 479), (187, 199, 97, 47, 477, 479), (187, 167, 97, 47, 477, 479), (187, 167, 97, 47, 408, 479), (187, 167, 97, 47, 408, 458), (187, 122, 97, 47, 408, 458), (187, 122, 28, 47, 408, 458), (125, 122, 28, 47, 408, 458), (125, 122, 28, 43, 408, 458)]
    p = p[::-1]
    get_figure(p, ['FR', 'FR', 'DK','DE','SE','SE','NL','NL'], ['ZE', 'ZR', 'AR', 'ER', 'TE', 'TI'], '')