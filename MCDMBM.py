#!/usr/bin/env python
# coding: utf-8

# In[1]:

import PySimpleGUI as sg
import xlsx_reader as rd 
from math import floor
import dijkstra as DKST
import pathplot
import pandas as pd
from numpy import array
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib
matplotlib.use('TkAgg')
from scipy.spatial import ConvexHull
from pathlib import Path
from copy import deepcopy
import ELECTREIII as E_III
import Promethee as P_II
import os 
#Allows for horinzontal scrollbar
class ScrolledText(sg.tk.Text):

    def __init__(self, master=None, **kw):

        horizontal_scrollbar = True
        self.frame = sg.tk.Frame(master)
        self.vbar = sg.tk.Scrollbar(self.frame)
        self.vbar.pack(side=sg.tk.RIGHT, fill=sg.tk.Y)

        if horizontal_scrollbar:
            self.hbar = sg.tk.Scrollbar(self.frame, orient='horizontal')
            self.hbar.pack(side=sg.tk.BOTTOM, fill=sg.tk.X)

        kw.update({'yscrollcommand': self.vbar.set})

        if horizontal_scrollbar:
            kw.update({'xscrollcommand': self.hbar.set})

        sg.tk.Text.__init__(self, self.frame, **kw)
        self.pack(side=sg.tk.LEFT, fill=sg.tk.BOTH, expand=True)
        self.vbar['command'] = self.yview

        if horizontal_scrollbar:
            self.hbar['command'] = self.xview

        # Copy geometry methods of self.frame without overriding Text
        # methods -- hack!
        text_meths = vars(sg.tk.Text).keys()
        methods = (
            vars(sg.tk.Pack).keys() |
            vars(sg.tk.Grid).keys() |
            vars(sg.tk.Place).keys())
        methods = methods.difference(text_meths)

        for m in methods:
            if m[0] != '_' and m != 'config' and m != 'configure':
                setattr(self, m, getattr(self.frame, m))

    def __str__(self):
        return str(self.frame)

class MCDMProblem():
    def __init__(self):
        self.qs = [] #Indifference thresholds for PII and ELECTRe
        self.ps = [] #preference thresholds for PII and ELECTRE
        self.vs = [] #vetos for ELECTRE
        self.ws = [] #weights for ELECTRE, PII, MAVT
        self.Us = [] #list of global values for MAVT
        self.midpoints = [] #list of mid points for bissection method
        self.init_E = []


# In[2]:
###---------------- ---------------------------- --------------------------------------
###---------------- FONCTIONS SECONDAIRES du GRAPH ------------------------------------
###---------------- ---------------------------- --------------------------------------

def crit(perfA, perfB):
    #which crit is increased from A to B
    #print(perfA, perfB)
    return [perfA[i] < perfB[i] for i in range(len(perfA))].index(True)

def ranking(x, eval):
    global MCDM, problem, m 
    if MCDM == 'PROMETHEII':
        E_temp = deepcopy(eval)
        E_temp.append(x)
        res = P_II.net_flow(E_temp,problem.ps,problem.qs,problem.ws)
        res = sorted(res)[::-1]
        i = 0
        while res[i][1] != len(E_temp)-1:
            i+=1
        nf = res[i][0]
        rang = [e[0] for e in res].index(nf) 
        return rang

    elif MCDM == 'ELECTRE':
        E_temp = deepcopy(eval)
        E_temp.append(x)
        cm = E_III.credibility_matrix(E_temp,problem.qs,problem.ps,problem.ws,problem.vs)
        SO_init, rangement = E_III.median_preorder(E_III.distillation_down(cm), E_III.distillation_upward(cm), m+1)
        return rangement.index(len(E_temp)-1)

    elif MCDM == 'MAVT':
        i = 0
        U = sum(x)
        while i < len(problem.Us) and problem.Us[i] > U:
            i+=1
        return i


def cibles(x,E, alts):
    global IMPROV_CRIT, MIN_RANK_INCREASE, MCDM, problem, DEGRAD_CRIT, n
    temp = {}
    minima = [10000]*n
    dk_max = [0]*n
    index = 0
    for a in E:
        for k in range(n):
            if a[k] > x[k] and a[k] <= IMPROV_CRIT[k]:
                evaluation = list(x)
                evaluation[k] = a[k]
                if MCDM == 'MAVT':
                    dk = a[k] - x[k] - distance_to_dominance(x, a)
                    if dk> 0 and (a[k] < minima[k] or (a[k] == minima[k] and dk > dk_max[k])) and ranking(x,E) - ranking(evaluation,E) >= MIN_RANK_INCREASE: #or (a[k] == minima[k] and dk == dk_max[k] and distance_to_dominance(a,x,k)) < dk_inv_max ):
                        minima[k] = a[k]
                        dk_max[k] = dk
                        temp[k] = [tuple(evaluation), alts[index]]
                elif MCDM == 'PROMETHEII':
                    wc = [a[l] if a[l] < x[l] else x[l] for l in range(n)]
                    wc = [DEGRAD_CRIT[l] if wc[l] < DEGRAD_CRIT[l] else wc[l] for l in range(n)]
                    wc[k] = a[k]
                    E_temp = deepcopy(E)
                    E_temp.append(x)
                    E_temp.append(wc)
                    pij = P_II.Pi(E_temp,problem.ps,problem.qs,problem.ws)
                    if P_II.pos_flow(pij,len(E_temp)-1) - P_II.pos_flow(pij,len(E_temp)-2) + P_II.neg_flow(pij,len(E_temp)-2)- P_II.neg_flow(pij,len(E_temp)-1) >=0:
                        dk = P_II.pos_flow(pij,len(E_temp)-1) - P_II.neg_flow(pij,len(E_temp)-1)
                    else:
                        dk = -1000
                    if dk>= 0 and (a[k] < minima[k] or (a[k] == minima[k] and dk > dk_max[k])) and ranking(x,E)- ranking(evaluation,E) >= MIN_RANK_INCREASE: #or (a[k] == minima[k] and dk == dk_max[k] and distance_to_dominance(a,x,k)) < dk_inv_max ):
                        minima[k] = a[k]
                        dk_max[k] = dk
                        temp[k] = [tuple(evaluation), alts[index]]
                elif MCDM == 'ELECTRE':
                    wc = [a[l] if a[l] < x[l] else x[l] for l in range(n)]
                    wc = [DEGRAD_CRIT[l] if wc[l] < DEGRAD_CRIT[l] else wc[l] for l in range(n)]
                    wc[k] = a[k]
                    Sab = E_III.credibility(wc,x, problem.qs, problem.ps, problem.ws, problem.vs)
                    Sba = E_III.credibility(x,wc, problem.qs, problem.ps, problem.ws, problem.vs)
                    if Sab - Sba > 0.05: #E_III.disc_thres(Sab):
                        dk = Sab
                    else:
                        dk = -1000
                    if dk>= 0 and (a[k] < minima[k] or (a[k] == minima[k] and dk > dk_max[k])) and ranking(x,E)- ranking(evaluation,E) >= MIN_RANK_INCREASE: #or (a[k] == minima[k] and dk == dk_max[k] and distance_to_dominance(a,x,k)) < dk_inv_max ):
                        minima[k] = a[k]
                        dk_max[k] = dk
                        temp[k] = [tuple(evaluation), alts[index]]
                
        index += 1
    return temp

def distance_to_dominance(x,y):
    global DEGRAD_CRIT
    res = sum([x[k]-y[k] if DEGRAD_CRIT[k] <= y[k] else x[k]-DEGRAD_CRIT[k] for k in range(n) if y[k] < x[k]])
    return res

def initial_graph(E,start, alts, net, dico_nodes):
    H = set()
    net.add_node(tuple(E[start]))
    net.add_node(None)
    net.add_edge(None,tuple(E[start]),0, None)
    H.add(tuple(E[start]))
    while H:
        next_H = set()
        for node in H:
            temp = cibles(node,E,alts)
            if temp:
                realistic = max_opportunities(temp,node,E)
                for k in temp:
                    obs = temp[k][1]
                    if temp[k][0] not in net.nodes:
                        net.add_node(temp[k][0])
                        next_H.add(temp[k][0])
                    if k in realistic:  
                        net.add_edge(node, temp[k][0], 0, obs)
                    else:
                        net.add_edge(node, temp[k][0], 1, obs)
                    dico_nodes[node, temp[k][0]] = obs
        H = next_H
    
def select_PO_sol(distance,l,val):
    indexes = []
    for i,elem in enumerate(distance):
        if elem == val:
            indexes.append(i)
    return indexes[l]

def rebuild_paths(index_sink, distance, prev, index_nodes, net, w_agg):
    sink = index_nodes[index_sink]
    incidents = set(prev[index_sink])
    paths = []
    for elem in incidents:
        paths.append([index_sink]+[elem])
    while incidents:
        #print(incidents)
        h = set()
        for elem in incidents:
            new_paths = []
            if prev[elem] != None:
                for a in prev[elem]:
                    #print("prevs",a)
                    h.add(a)
                    for i in range(len(paths)):
                        #print(elem,paths[i][-1])
                        if paths[i][-1] == elem:
                            if a not in paths[i]:
                                new_paths.append(paths[i][:]+[a])
                            #print("paths_in",paths)
                            #print(new_paths)
                paths = [a for a in paths if a[-1] != elem]
                for new in new_paths:
                    paths.append(new)
                #print("paths after loop", paths)
        incidents = h
    return [[index_nodes[elem][1] for elem in p] for p in paths]
        


def rebuild_path(index_sink, distance, prev, index_nodes, net, w_agg):
    global E_v
    sink = index_nodes[index_sink]
    path = [sink[1]]
    S = 0
    C = 0
    res = 0
    U = 0
    while 1:
        Ut = ranking(sink[0],E_v) - ranking(sink[1],E_v)
        U += Ut
        S_now = (net.weights[sink])
        S += S_now*Ut

        crit_1 = crit(sink[0],sink[1])
        index_sink = prev[index_sink][0]
        temp = sink
        sink = index_nodes[index_sink]
        if sink[0] != None:
            crit_2 = crit(sink[0],sink[1])
        else:
            crit_2 = crit_1
        if (net.observed[temp] == net.observed[sink]) or (crit_1 == crit_2):
            consistency = 0
        else:
            consistency = 1
        C += consistency*Ut
        path.append(sink[1])
        res += Ut*(1 - w_agg[0]*S_now - w_agg[1]*consistency)
        if sink[0] == None:
            break;
        U_2 = ranking(path[-1],E_v) - ranking(path[0],E_v)
        assert U == U_2    
    else:
        path = [tuple(E[start])]
        S, C, U = 1, 1, 0
    return path, S, C, U

def max_opportunities(temp,node,E):
    realistic = []
    S_max = 0
    for k in temp:
        S = supporting_alternatives(node, E, k)
        if S > S_max:
            realistic = [k]
            S_max = S
        elif S == S_max:
            realistic.append(k)
    return realistic

def supporting_alternatives(y,E, k):
    global problem, MCDM, n 
    count = 0
    for elem in E:
        if elem[k] > y[k]:
            if MCDM == 'MAVT':
                dk = elem[k] - y[k]- distance_to_dominance(y,elem)
                if dk > 0:
                #print(Alt[E.index(elem)], 'crit', k, 'dk', dk)
                    count+=1
            elif MCDM == 'PROMETHEII':
                wc = [elem[l] if elem[l] < y[l] else y[l] for l in range(n)]
                wc[k] = elem[k]
                E_temp = deepcopy(E)
                E_temp.append(y)
                E_temp.append(wc)
                pij = P_II.Pi(E_temp,problem.ps,problem.qs,problem.ws)
                if P_II.pos_flow(pij,len(E_temp)-1) - P_II.pos_flow(pij,len(E_temp)-2) + P_II.neg_flow(pij,len(E_temp)-2)- P_II.neg_flow(pij,len(E_temp)-1) >=0:    #print(Alt[E.index(elem)], 'crit', k, 'dk', dk)
                    count+=1
            elif MCDM == 'ELECTRE':
                wc = [elem[l] if elem[l] < y[l] else y[l] for l in range(n)]
                wc[k] = elem[k]
                Sab = E_III.credibility(wc,elem, problem.qs, problem.ps, problem.ws, problem.vs)
                Sba = E_III.credibility(elem,wc,problem.qs, problem.ps, problem.ws, problem.vs)
                if Sab - Sba > E_III.disc_thres(Sab):
                    dk = Sab
                else:
                    dk = -1000
                if dk >= 0:
                #print(Alt[E.index(elem)], 'crit', k, 'dk', dk)
                    count+=1
    return count

###---------------- ---------------------------- --------------------------------------
###---------------- FONCTIONS PRIMAIRES du GRAPH --------------------------------------
###---------------- ---------------------------- --------------------------------------

# In[4]:


def network(E, start, alts,w_agg):
    global MIN_RANK_INCREASE
    net = DKST.Graph()
    dico_nodes = {}
    dico_nodes[None,tuple(E[start])] = None
    initial_graph(E, start, alts, net, dico_nodes)

    #METAGRAPH:
    index_nodes = {}
    index = 0
    Adj = [[] for _ in range(len(net.weights.keys())+1)]
    edges_visited ={}
    for edge in net.weights:
        if edge not in edges_visited:
            index+=1
            edges_visited[edge] = index
            index_nodes[index] = edge
        if edge[0] == None:
            src = index
        
        center = edge[1]
        for next in net.edges[center]:
            if (center, next) not in edges_visited:
                index+=1
                index_nodes[index] = (center, next)
                edges_visited[(center, next)] = index
            crit_2 = crit(center,next)
            if edge[0] != None:
                U = (ranking(center,E)-ranking(edge[0],E), ranking(next,E)-ranking(center,E))
                crit_1 = crit(edge[0],center)
                S = (net.weights[edge],net.weights[(center,next)])            
                if (net.observed[edge] == net.observed[(center,next)]) or (crit_1 == crit_2):
                    consistency = 0
                else:
                    consistency = 1
            else:
                U = (0, ranking(next,E)-ranking(center,E))
                S = (0,net.weights[(center,next)])
                consistency = 1
            w = U[1]*(1 - S[1]*w_agg[0] - consistency*w_agg[1])
            Adj[edges_visited[edge]].append((edges_visited[(center, next)], w ))

    return net, src, Adj, dico_nodes, index_nodes , edges_visited

def paths_max_Smin(Adj, index_nodes, edges_visited, E,start,alts,src,net,w_agg,file_paths,file_indic):
    paths = []
    if len(Adj) > 2:
        #st = time.process_time()
        distance, prev = DKST.simple_dijkstra(Adj,src)
        value_to_outlook = min(distance)
        #value_to_outlook = sorted(distance)[10]
        nb_sols = distance.count(value_to_outlook)
        #print("OBJECTIVE VALUE",value_to_outlook)
        #print("# PO solutions", nb_sols)
        real_nb_sols = 1

        for l in range(nb_sols):
            index_sink = select_PO_sol(distance,l,value_to_outlook)
            print("#SOL n°",l)
            new_paths = rebuild_paths(index_sink, distance, prev, index_nodes, net, w_agg)
            print(new_paths)
            _, S, C, U = rebuild_path(index_sink, distance, prev, index_nodes, net, w_agg)
            #path, S, C, U = rebuild_path(index_sink, distance, prev, index_nodes, net, w_agg)
            #path = path[::-1]
            for path in new_paths:
                path = path[::-1]
                if path not in paths:
                    paths.append(path)
                    #print("SOLUTION NUMERO",str(real_nb_sols))
                    describe_path(path,alts, E, net,w_agg,file_paths,file_indic)
                    real_nb_sols +=1

    else:
        paths, S, C, U = [[]], 0, 0, 0

    return path, S, C, U

def describe_path(path,Alt,E,graph,w_agg,file_paths,file_indic):
    global G, nb_paths, TABLE_PATHS, penalty_weights, viz_path
    if len(path) > 1:
        F = 0
        crit_prec = None
        count = 1
        steps = 1
        prec_obs = None
        dp =[]
        dp_GUI = []
        S = 0
        C = 0
        U = 0
        #R3.append("\t".join([str(path[0][k]) for k in range(len(path[0]))]))
        print(path)
        ref_alts = []
        for i in range(1,len(path)):
            ref_alts.append(graph.observed[(path[i-1],path[i])])
            current_crit = [k for k in range(n) if path[i][k] > path[i-1][k]]
            ind = Alt.index(graph.observed[(path[i-1],path[i])])
            #R3.append("\t".join([str(path[i][k]) for k in range(len(path[0]))]))
            DU = ranking(path[i-1],E)-ranking(path[i],E)
            U += DU
            print(DU)
            if graph.weights[(path[i-1],path[i])] == 0:
                #print('error11')
                #print(graph.observed[(path[i-1],path[i])])
                #print(G[current_crit[0]+1])
                dp_GUI.append(graph.observed[(path[i-1],path[i])]+ " - " + G[current_crit[0]+1] + " +("+str(DU)+')')
                #print('error12')
                print('step'+str(i),graph.observed[(path[i-1],path[i])], G[current_crit[0]+1], E[ind][current_crit[0]],"R_diff",str(DU))
                dp.append(graph.observed[(path[i-1],path[i])]+ " - " + G[current_crit[0]+1] + " +("+str(DU)+')')
            else:
                S += DU
                #print('error21')
                dp_GUI.append(graph.observed[(path[i-1],path[i])]+ " - " + G[current_crit[0]+1] + " +("+str(DU)+'!B!)')
                #print('error22')
                dp.append(graph.observed[(path[i-1],path[i])]+ " - " + G[current_crit[0]+1] + " (+"+str(DU)+'!B!)')
                #print('step'+str(i),graph.observed[(path[i-1],path[i])], G[current_crit[0]+1], E[ind][current_crit[0]],"R_diff",str(DU), " ! BOTTLENECK!")
            if (prec_obs != graph.observed[(path[i-1],path[i])] or prec_obs == None)  and (current_crit != crit_prec or crit_prec == None) :
                count+=1
                C += DU
            else:
                #count += 1
                steps += 1
                F += count
                count = 1
            prec_obs = graph.observed[(path[i-1],path[i])]
            crit_prec = current_crit

        F = F/steps
        res = "\t".join(dp) 
        if res in paths_evaluated:
            index = paths_evaluated.index(res)
            viz_path[index][2].append(w_agg)
        else:
            nb_paths += 1
            paths_evaluated.append(res)
            index = len(paths_evaluated) - 1
            dp_GUI.extend([str(S),str(C),str(U)])
            TABLE_PATHS.append(dp_GUI)
            viz_path.append((tuple([p for p in path]),tuple(ref_alts), [w_agg]))
            file_paths.write("p_"+str(index)+ "\t" + res + "\n")

            file_indic.write("p_"+str(index)+ "\t" + str(S) + "\t" + str(C) + "\t" + str(U) + "\n")
        penalty_weights[w_agg].append(str(index))
        print("||#R risking bottleneck:",str(S),"||#R OP change:", C, "||Focus:", F,"||#R earned:", U)
    else:
        print(1, 0, 0, 0)



def solve_instance(E, start, alts,w_agg,file_paths, file_indic):
    graph,src, Adj, dico_nodes, dico_index, edges_visited = network(E, start, alts,w_agg)
    nb_nodes_graph, nb_edges_graph, nb_nodes_meta,nb_edges_meta = len(graph.nodes), len(graph.weights),len(graph.weights),sum([len(elem) for elem in Adj])
    print(nb_nodes_graph, nb_edges_graph, nb_nodes_meta,nb_edges_meta)
    if nb_edges_meta <= 1:
        sg.popup_quick_message('No improvement possible!', auto_close=True, non_blocking=True, font='Default 18')
    else:
        path, S, C ,U = paths_max_Smin(Adj, dico_index, edges_visited, E, start, alts ,src,graph,w_agg,file_paths,file_indic)
    return nb_edges_meta > 1

###---------------- ---------------------------- --------------------------------------
###---------------- Visuel DU PROGRAMME  --------------------------------------
###---------------- ---------------------------- --------------------------------------

def model_param(start):
    global problem, MCDM, E_v,n, G
    if MCDM == 'MAVT' and problem.ws == []:
        k = 0
        while k < n:
            try:
                problem.ws.append(float(sg.popup_get_text('Normalized scaling factor for '+ G[k+1] + '(between 0 and 1)', default_text = '').strip()))
                k+=1
            except:
                sg.popup_quick_message('Please insert a valid value', auto_close=True, non_blocking=True, font='Default 18')
    if (MCDM == 'PROMETHEII' or MCDM == 'ELECTRE') and problem.ps == [] :
        k = 0
        while k < n:
            try:
                problem.ps.append(float(sg.popup_get_text('Strict preference threshold for '+ G[k+1] + ' (MIN = ' +str(MINIMUMS[k])+' , ' + 'MAX = '+str(MAXIMUMS[k]) + ')', default_text = str((MAXIMUMS[k]-MINIMUMS[k])/2)).strip()))
                k+=1
            except:
                sg.popup_quick_message('Please insert a valid value', auto_close=True, non_blocking=True, font='Default 18')
        k = 0
        while k < n:
            try:
                problem.qs.append(float(sg.popup_get_text('Indifference threshold for '+ G[k+1] + ' (MIN = ' +str(MINIMUMS[k])+' , ' + 'MAX = '+str(MAXIMUMS[k]) + ')', default_text = str((MAXIMUMS[k]-MINIMUMS[k])/2)).strip()))
                k+=1
            except:
                sg.popup_quick_message('Please insert a valid value', auto_close=True, non_blocking=True, font='Default 18')
        k = 0
        while k < n:
            try:
                problem.ws.append(float(sg.popup_get_text('Normalized weight for '+ G[k+1] + '(between 0 and 1)', default_text = '').strip()))
                k+=1
            except:
                sg.popup_quick_message('Please insert a valid value', auto_close=True, non_blocking=True, font='Default 18')

    if MCDM == 'ELECTRE' and problem.vs == []:
        k = 0
        while k < n:
            try:
                problem.vs.append(float(sg.popup_get_text('Veto threshold for '+ G[k+1] + ' (MIN = ' +str(MINIMUMS[k])+' , ' + 'MAX = '+str(MAXIMUMS[k]) + ')', default_text = str((MAXIMUMS[k]-MINIMUMS[k])/2)).strip()))
                k+=1
            except:
                sg.popup_quick_message('Please insert a valid value', auto_close=True, non_blocking=True, font='Default 18')




def weights_zone(ind):
    global TABLE_WEIGHTS, penalty_weights
    fig = matplotlib.figure.Figure(figsize=(5, 4), dpi=70)
    points = []
    for key in penalty_weights:
        print(type(key),key)
        if str(int(ind)-1) in penalty_weights[key]:
            points.append(key)
    points = array(points)
    print(points)
    try:
        hull = ConvexHull(points)
        corners=[]
        for simplex in hull.simplices:
            corners+=list(simplex)
        print(corners)
        ax = fig.add_subplot(111)
        #ax.plot(points[corners, 0], points[corners, 1], 'm-',alpha=0.3)
        #ax.plot(points[hull.vertices,0], points[hull.vertices,1], 'r--', lw = 2)
        ax.fill(points[hull.vertices,0], points[hull.vertices,1], 'g',alpha=0.1)
    except:
        ax = fig.add_subplot(111)
        ax.plot([c[0] for c in points],[c[1] for c in points],'x')
    ax.plot([0,1],[1,0], linestyle='--')
    ax.set_title('Optimality of p' + ind + ' (in function of the penalty weights)')
    ax.set_xlim([0.1,1])
    ax.set_ylim([0.1,1])
    ax.set_ylabel('Operational change penalty')
    ax.set_xlabel('Risk of bottleneck penalty')
    return fig 




# ------------------------------- Beginning of Matplotlib helper code -----------------------

def draw_figure(canvas, figure,fig_canvas):
    try: 
        fig_canvas.get_tk_widget().pack_forget()
    except: 
        pass 
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg 

def compute_centroide(poids):
    x,y = 0, 0
    nb = 0
    for elem in poids:
        x += elem[0]
        y += elem[1]
        nb += 1
    return (round(x/nb,2), round(y/nb,2))

def bisection():
    global G, E_v, problem, MINIMUMS, MAXIMUMS, n
    sg.set_options(element_padding=(0, 0))
    v = [0, 25, 50, 75, 100]
    MAX_ROWS = 5
    MAX_COL = len(G) - 1
    if problem.midpoints == []:
        problem.midpoints = [[MINIMUMS[k] + (elem/100)*(MAXIMUMS[k]-MINIMUMS[k]) for k in range(n)] for elem in v]
    title = [sg.Text('Mid-points', size=(10, 1), justification='right',font=("Helvetica", 9))] + [sg.Text(G[j+1], size=(10, 1), justification='right', font=("Helvetica", 9)) for j in range(MAX_COL)]
    columm1 =  [title] + [[sg.Text('v'+str(v[i]), size=(10, 1), justification='right')] + [sg.InputText(default_text = "{0:.3f}".format(problem.midpoints[i][j]),size=(10, 1), pad=(1,1),border_width=0, justification='right', key=(i, j)) for j in range(MAX_COL)] for i in range(MAX_ROWS)]

    layout = [[sg.Col(columm1, size=(800, 500), scrollable=True,)], [sg.Button('Submit',key = 'SubmitValues')]]
    window = sg.Window(title="Bisection method",resizable=True).Layout(layout)
    while True:
        event, values = window.read(timeout = 100)       # type: str, dict
        if event == sg.WIN_CLOSED or event == 'Exit' or event is None:
            break
        elif event == 'SubmitValues':
            try: 
                problem.midpoints = [[float(values[(i,j)]) for j in range(MAX_COL)] for i in range(MAX_ROWS)]
                E_v = transform(problem.init_E,problem.midpoints)
                break
            except:
                sg.popup_quick_message('There is an error. Please verify the midpoint values', auto_close=True, non_blocking=True, font='Default 18')
    #for i in range(len(E_v)):
    #    for j in range(len(E_v[0])):
    #        E_v[i][j] = int(E_v[i][j]*100000)/100000
    window.Close()

def performance_bounds():
    global G, problem, n, MAXIMUMS, MINIMUMS
    sg.set_options(element_padding=(0, 0))
    MAX_ROWS = 2
    MAX_COL = len(G) - 1
    label = ['Worst', 'Best']
    if problem.DEGRAD_CRIT == []:
        problem.DEGRAD_CRIT = MINIMUMS[:]
    if problem.IMPROV_CRIT == []:
        problem.IMPROV_CRIT = MAXIMUMS[:]
    if problem.ws == []:
        problem.ws = [1/n]*n
    val = [problem.DEGRAD_CRIT,problem.IMPROV_CRIT]
    annonce = [sg.Text('Please, insert the worst and best possible performance reachable for each criterion:',font=("Helvetica", 9))]
    title = [sg.Text('', size=(10, 1), justification='right',font=("Helvetica", 9))] + [sg.Text(G[j+1], size=(10, 1), justification='right', font=("Helvetica", 9)) for j in range(MAX_COL)]
    columm1 =  [title] + [[sg.Text(label[i], size=(10, 1), justification='right')] + [sg.InputText(default_text = "{0:.3f}".format(val[i][j]),size=(10, 1), pad=(1,1),border_width=0, justification='right', key=(i, j)) for j in range(MAX_COL)] for i in range(MAX_ROWS)]

    annonce2 = [sg.Text('Please, insert the normalized importance weights or scaling factors for each criterion:',font=("Helvetica", 9))]
    title2 = [sg.Text('', size=(10, 1), justification='right',font=("Helvetica", 9))] + [sg.Text(G[j+1], size=(10, 1), justification='right', font=("Helvetica", 9)) for j in range(MAX_COL)]
    columm2 =  [title2] + [[sg.Text('Weight', size=(10, 1), justification='right')] + [sg.InputText(default_text = "{0:.3f}".format(problem.ws[j]),size=(10, 1), pad=(1,1),border_width=0, justification='right', key=(2, j)) for j in range(MAX_COL)]]

    layout = [annonce, [sg.Col(columm1, size=(800, 300), scrollable=True,)], annonce2, [sg.Col(columm2,size=(800,300), scrollable = True)], [sg.Button('Submit',key = 'SubmitBounds')]]
    window = sg.Window(title="Performance bounds ",resizable=True).Layout(layout)
    while True:
        event, values = window.read(timeout = 100)       # type: str, dict
        if event == sg.WIN_CLOSED or event == 'Exit' or event is None:
            break
        elif event == 'SubmitBounds':
            try: 
                problem.DEGRAD_CRIT = [float(values[(0,j)]) for j in range(MAX_COL)]
                problem.IMPROV_CRIT = [float(values[(1,j)]) for j in range(MAX_COL)]
                problem.ws = [float(values[(2,j)]) for j in range(MAX_COL)]
                print("bounds", problem.DEGRAD_CRIT,problem.IMPROV_CRIT)
                break
            except:
                sg.popup_quick_message('There is an error. Please verify the values you entered', auto_close=True, non_blocking=True, font='Default 18')
    window.Close()
def transform(E,midpoints):
    mm = len(E)
    v = [0, 0.25, 0.50, 0.75, 1]
    #weights =[0.1, 0.2, 0.2, 0.2, 0.2, 0.1]
    if type(E[0]) == list:
        nn = len(E[0])
        res = [[0]*nn for l in range(mm)]
        for i in range(mm):
            for j in range(nn):
                if E[i][j] >= midpoints[-1][j]:
                    res[i][j] = 1
                elif E[i][j] > midpoints[0][j]:
                    l = 0
                    while E[i][j] <= midpoints[l][j]:
                        l+=1
                    res[i][j] = linear_transform(E[i][j], (v[l-1],midpoints[l-1][j]), (v[l],midpoints[l][j]))
    else:
        res = [0 for l in range(mm)]
        for j in range(mm):
            if E[j] >= midpoints[-1][j]:
                res[j] = 1
            elif E[j] > midpoints[0][j]:
                l = 0
                while E[j] <= midpoints[l][j]:
                    l+=1
                res[j] = linear_transform(E[j], (v[l-1],midpoints[l-1][j]), (v[l],midpoints[l][j]))

    return res


def linear_transform(val, inf, sup):
    return inf[0] + (sup[0]-inf[0])*(val - inf[1])/(sup[1] - inf[1])

def optimization_dir(E):
    global n, G
    sg.set_options(element_padding=(0, 0))
    k = 0
    minimize = [0]*n
    window = sg.Window(title = "Optimization direction",modal=True, resizable = True, layout = [[sg.Text("Maximize " + G[k+1] + " ? (Yes = 'y'; No = 'n'):")] +  [sg.InputText(default_text = 'y',size=(2, 1), pad = (1, 1), key = str(k))] for k in range(n)] + [[sg.Button('continue',key='submit')]])
    while True:
        event, values = window.read(timeout = 100)
        if event == "Exit" or event == sg.WIN_CLOSED:
            break
        if event == 'submit':
            try:
                for k in range(n):
                    if values[str(k)].strip().lower() == 'n':
                        minimize[k] = 1
                break
            except:
                sg.popup_quick_message('Please only accepted answers : y or n', auto_close=True, non_blocking=True, font='Default 18')
    window.close()
    for elem in E:
        for k in range(n):
            if minimize[k] == 1:
                elem[k] *= -1

def save_casestudy():
    """ save the main variables in the following order: [n, m, DM_name,len(problem.midpoints)], problem.init_E, E_v, problem.midpoints, problem.DEGRAD_CRIT, 
    problem.IMPROV_CRIT, problem.ws"""
    global problem, n, m, E_v, DM_name, Alt, G
    foldername = sg.PopupGetFolder('', no_window=True)
    filename = sg.PopupGetFile('Please enter a file name to save your congif')
    pathname = os.path.join(foldername ,filename + '.txt')
    f = open(pathname,'w')
    print(problem.init_E,'\n', E_v,'\n', problem.midpoints, '\n', problem.DEGRAD_CRIT)
    try:
        f.write(str(n)+ ';' + str(m) +';' + str(DM_name)+';'+str(len(problem.midpoints))+ '\n')
        for elem in problem.init_E:
            f.write(";".join([str(a) for a in elem])+'\n')
        for elem in E_v:
            f.write(";".join([str(a) for a in elem])+'\n')
        for elem in problem.midpoints:
            f.write(";".join([str(a) for a in elem])+'\n')
        f.write(";".join([str(a) for a in problem.DEGRAD_CRIT])+'\n')
        f.write(";".join([str(a) for a in problem.IMPROV_CRIT])+'\n')
        f.write(";".join([str(a) for a in problem.ws])+'\n')
        f.write(";".join(Alt)+'\n')
        f.write(";".join(G)+'\n')
    
    except:
        sg.popup_quick_message('Only fully complete config files are accepted', auto_close=True, non_blocking=True, font='Default 18')
    f.close()


def load_casestudy():
    global problem, n, m , E_v, DM_name, Alt, G
    pathname = sg.PopupGetFile('file to open', no_window=True, file_types=(("text files","*.txt"),))
    f = open(pathname,'r')
    try :
        n, m, DM_name, n_mid = f.readline().split(';')
        n, m, n_mid = int(n), int(m), int(n_mid)
        problem.init_E = [[float(a) for a in f.readline().split(';')] for i in range(m)]
        problem.E_v = [[float(a) for a in f.readline().split(';')] for i in range(m)]
        problem.midpoints = [[float(a) for a in f.readline().split(';')] for i in range(n_mid)]
        problem.DEGRAD_CRIT = [float(a) for a in f.readline().split(';')]
        problem.IMPROV_CRIT = [float(a) for a in f.readline().split(';')]
        problem.ws = [float(a) for a in f.readline().split(';')]
        Alt = f.readline().split(';')
        G = f.readline().split(';')

    except:
        sg.popup_quick_message('Only fully complete config files are accepted', auto_close=True, non_blocking=True, font='Default 18')

# ------------------------------- Beginning of GUI CODE -------------------------------


def visualization():
    global DM_name, transformed, discretized, problem, MCDM, MIN_RANK_INCREASE, MINIMUMS, MAXIMUMS, DEGRAD_CRIT, IMPROV_CRIT, PATH_FILES,viz_path, fig_canvas_agg, browse_path, headers_paths, headers_weights, values_displayed, G , Alt, V0, V1, V2, V3, V4, V5, w_aggs, p_v,m, n, E_v, TABLE_PATHS, TABLE_WEIGHTS, DM_name, start,nb_paths, paths_evaluated,penalty_weights
    options = ['Better to face a bottleneck than an operational change',
               'Better to make an operational change than to face a bottleneck',
               'Both are equally bad',
               'I have no preference on this matter'
                ]
    models = ['PROMETHEII', 'MAVT', 'TOPSIS', 'ELECTRE']
    models = ['MAVT']
    if V4:
        MESSAGE = ''
    else:
        MESSAGE = 'No path generated yet'
    menu_def = [['&File', ['&Open', '&Save']]]
    tab0_layout = [[sg.Text('Select one of the following MCDM models:')],[sg.Listbox(values = models, enable_events=True,key='_LBMODEL_',size=(70,5))],[sg.Submit(key='Submit_model')]]

    tab1_layout = [[sg.Text("Open a performance table (.xlsx only!)")], [sg.Input(browse_path, key='IN'), sg.FileBrowse(target='IN')],
              [sg.Submit(key='Submit_1')],
              [sg.Text("Select a folder to save the results"), sg.Input(key='IN2',change_submits=True), sg.FolderBrowse(target='IN2')],
              [sg.Submit(key='Submit_1bis')]]

    tab2_layout = [[sg.Table(values=values_displayed, headings=G, key= '-table1-', enable_events = True, display_row_numbers=True,
                            auto_size_columns=False,justification='left',vertical_scroll_only = False, alternating_row_color='olive drab',num_rows=min(25,len(values_displayed)))],
            [sg.Text('Alternative selected to build a strategy of improvement:')],[sg.Input(key='sr',default_text = DM_name)],[sg.Submit(key='Submit_2')],
            [sg.Text('1) Introduce the Optimization direction of each criterion:')],[sg.Button('Select Optimization direction', key = 'optimization')],
            [sg.Text('2) MAVT only - Transform the performances in partial values (bisection method):'), sg.Button('Transform', key = 'bisection')]]

    tab3_layout = [[sg.Text('Select the closest proposition to your preference:')],[sg.Listbox(values = options, enable_events=True,key='_LB2_',size=(70,5))],[sg.Submit(key='Submit_3')]]
    tab4_layout = [[sg.Text("Please analyze the following efficient scenarios:")],
              [sg.Table(values=TABLE_PATHS, key='_tablepath_', enable_events = True, headings=headers_paths,alternating_row_color='olive drab', auto_size_columns = True,vertical_scroll_only = False, hide_vertical_scroll = False, num_rows=min(25,len(TABLE_PATHS)))],
              [sg.Button('Generate paths',key = 'gen'), sg.Text(MESSAGE,key='_Status_',size =(10,2))], [sg.Button('Visualize the selected path:',key = '_vizpath_'),  sg.Text('',key='_selectedpath_',size =(5,2))]]

    weight_res = [[sg.Table(values=TABLE_WEIGHTS, headings=headers_weights, alternating_row_color='olive drab',auto_size_columns = True,vertical_scroll_only = False, hide_vertical_scroll = False, num_rows=min(10,len(TABLE_WEIGHTS)))]]
    weight_plot = [[sg.Canvas(key='-CANVAS-')]]
    tab5_layout = [[sg.Text("Optimality of a path for each set of penalty weights (a,b) \n where a is the penalty for the amount of bottlenecks \n and b for the operational changes:")],
                    [sg.Column(weight_res,size = (600,400)),sg.Column(weight_plot)],
              #[sg.Table(values=TABLE_WEIGHTS, headings=headers_weights, alternating_row_color='olive drab',auto_size_columns = True,vertical_scroll_only = False, hide_vertical_scroll = False, num_rows=min(25,len(TABLE_WEIGHTS))),
              #sg.Canvas(key='-CANVAS-')],
              [sg.Text('Optimality area for p '), sg.Input(key='-Draw_ind-'), sg.Button('Draw Plot ',key = '-Draw-')]]
    # The TabgGroup layout - it must contain only Tabs
    tab_group_layout = [
                    [sg.Tab('MCDM model', tab0_layout, visible = V0, font='Courier 15', key='-MODELS-' ),
                     sg.Tab('Browse', tab1_layout, visible = V1, font='Courier 15', key='-BROWSE-'),
                     sg.Tab('Data', tab2_layout, visible= V2, key='-DATA-'),
                     sg.Tab('Risks', tab3_layout, visible = V3, key='-RISKS-'),
                     sg.Tab('Paths', tab4_layout, visible= V4, key='-PATHS-'),
                     sg.Tab('Weights Analysis', tab5_layout, visible= V5, key= '-WEIGHTS-')
                     ]]
    layout = [[sg.Menu(menu_def, tearoff=True)],[sg.TabGroup(tab_group_layout,
                       enable_events=True,
                       key='-TABGROUP-')]]
    window = sg.Window(title="MC strategy GEN",resizable=True).Layout(layout)
    while True:
        event, values = window.read(timeout = 100)       # type: str, dict
        if event == sg.WIN_CLOSED or event == 'Exit' or event is None:
            break
        if event =='Save':
            save_casestudy()
            window.Close()
            visualization()
        if event =='Open':
            load_casestudy()
            values_displayed = []
            for i in range(len(E_v)):
                values_displayed.append([Alt[i]] + ["{0:.3f}".format(elem) for elem in E_v[i]])
            window.Close()
            visualization()
        if event == 'Submit_model':
            if values['_LBMODEL_'] == []:
                sg.popup_quick_message('Please select a model!', auto_close=True, non_blocking=True, font='Default 18')
            else:
                MCDM = values['_LBMODEL_'][0]
        if event == 'optimization':
            optimization_dir(problem.init_E)
            E_v = deepcopy(problem.init_E)
            print(E_v, Alt ,G)
            m = len(E_v)
            n = len(E_v[0]) 
            MAXIMUMS = [(-1)*float('inf')]*n
            MINIMUMS = [float('inf')]*n
            for i in range(len(problem.init_E)):
                for j in range(len(problem.init_E[0])):
                    if problem.init_E[i][j] < MINIMUMS[j]:
                        MINIMUMS[j] = problem.init_E[i][j]
                    elif problem.init_E[i][j] > MAXIMUMS[j]:
                        MAXIMUMS[j] = problem.init_E[i][j]
            V2 = True
            values_displayed = []
            for i in range(len(E_v)):
                values_displayed.append([Alt[i]] + ["{0:.3f}".format(elem) for elem in E_v[i]])
            print(values_displayed)
            window.close()
            visualization()

        if event == 'Submit_1':
            if values['IN']:
                browse_path = values['IN']
                try:
                    problem.init_E, Alt,G = rd.get_data(browse_path)
                    print(E_v, Alt ,G)
                    m = len(problem.init_E)
                    n = len(problem.init_E[0]) 
                    V2 = True
                    values_displayed = []
                    for i in range(len(problem.init_E)):
                        values_displayed.append([Alt[i]] + ["{0:.3f}".format(elem) for elem in problem.init_E[i]])
                    print(values_displayed)
                    window.close()
                    visualization()
                except:
                    sg.popup_quick_message('Please select a valid file!', auto_close=True, non_blocking=True, font='Default 18')

                
            else:
                sg.popup_quick_message('Please select a file!', auto_close=True, non_blocking=True, font='Default 18')
        elif event == 'Submit_1bis':
            if values['IN2']:
                PATH_FILES = values['IN2']
            else:
                sg.popup_quick_message('Please select a folder!', auto_close=True, non_blocking=True, font='Default 18')
        elif event == '-table1-':
            selected_row = values['-table1-'][0]
            print(selected_row)
            window.FindElement('sr').Update(value = Alt[int(selected_row)])
            window.Refresh()
        elif event == 'bisection':
            if MCDM == 'MAVT':
                try:
                    assert MINIMUMS != []
                    bisection()
                    transformed = True
                    values_displayed = []
                    for i in range(len(E_v)):
                        values_displayed.append([Alt[i]] + [ "{0:.3f}".format(elem) for elem in E_v[i]])
                    print(values_displayed)
                    window.close()
                    visualization()
                except:
                    sg.popup_quick_message('Something went wrong. Did you enter the optimization direction of each criterion?', auto_close=True, non_blocking=True, font='Default 18')
        elif event == 'Submit_2':
            if values['sr'] == '':
                sg.popup_quick_message('Please select an alternative in the list!', auto_close=True, non_blocking=True, font='Default 18')
            else:
                try:
                    DM_name = Alt[int(values['-table1-'][0])]
                except:
                    sg.popup_quick_message('Please select an alternative in the list!', auto_close=True, non_blocking=True, font='Default 18')
                V3 = True
                start = Alt.index(DM_name)
                #model_param(start)
                window['-RISKS-'].update(visible=V3)
        elif event == 'Submit_3':
            if values['_LB2_'] == []:
                sg.popup_quick_message('Please select a statement!', auto_close=True, non_blocking=True, font='Default 18')
            else:
                select_option = values['_LB2_']
                case = options.index(select_option[0])
                w_aggs= [(j/10, i/10) for i in range(1,11) for j in range(1, 11-i)]
                remove_items = []
                for elem in w_aggs:
                    if case == 0:
                        if elem[0]<elem[1]:
                            remove_items.append(elem)
                    elif case ==1:
                        if elem[0]>elem[1]:
                            remove_items.append(elem)
                    elif case == 2:
                        if elem[0]<elem[1]-0.1 or elem[0]>elem[1]+0.1:
                            remove_items.append(elem)
                for elem in remove_items:
                    w_aggs.remove(elem)
                print(len(w_aggs))
                V4 = True
                window['-PATHS-'].update(visible=V4)
        elif event == 'gen':
            TABLE_PATHS = []
            TABLE_WEIGHTS = []
            paths_evaluated = []
            viz_path = []
            penalty_weights = {}
            #DEGRAD_CRIT = []
            performance_bounds()
            correct_val = False
            while not correct_val:
                try:
                    MIN_RANK_INCREASE = int(sg.popup_get_text('What is the minimum rank increase for a step ?', default_text = MIN_RANK_INCREASE).strip())
                    correct_val = True
                except:
                    sg.popup_quick_message('Please insert a positive integer number', auto_close=True, non_blocking=True, font='Default 18')
            if MCDM == 'MAVT' and transformed:
                E_v = transform(problem.init_E,problem.midpoints)
                E_v = [[elem[k]*problem.ws[k] for k in range(n)] for elem in E_v]
                print(E_v)
                IMPROV_CRIT = transform(problem.IMPROV_CRIT,problem.midpoints)
                DEGRAD_CRIT = transform(problem.DEGRAD_CRIT,problem.midpoints)
                DEGRAD_CRIT = [DEGRAD_CRIT[k]*problem.ws[k] for k in range(n)]
                IMPROV_CRIT = [IMPROV_CRIT[k]*problem.ws[k] for k in range(n)]
                print("bounds", DEGRAD_CRIT,IMPROV_CRIT)
                p_v= [sum(a) for a in E_v if a != E_v[start]]
                problem.Us = sorted(p_v)[::-1]
            nb_paths = 0
            V5 = True
            data_folder = Path(PATH_FILES)
            file1 = DM_name+"_paths_"+MCDM+".txt"
            file2 = DM_name+"_weights_"+MCDM+".txt"
            file3 = DM_name+"_indicateurs_"+MCDM+".txt"

            base1 = data_folder /file1
            base2 = data_folder /file2
            base3 = data_folder /file3
            try:
                file_paths = open(base1,'w')
                file_weights = open(base2,'w')
                file_indic = open(base3,'w')
            except:
                file_paths = open(DM_name+"_paths_"+MCDM+".txt",'w')
                file_weights = open(DM_name+"_weights_"+MCDM+".txt",'w')
                file_indic = open(DM_name+"_indicateurs_"+MCDM+".txt",'w')
            sg.popup_quick_message('Hang on for a moment, this will take a bit to create....', auto_close=True, non_blocking=True, font='Default 18')
            ind = 0
            for w_agg in w_aggs:
                #window.Element('_Status_').Update('Work done: ' + str(ind/len(w_aggs)) +'%')
                window.FindElement('_Status_').Update(value='Work done: ' + str(int(100*(ind/len(w_aggs)))) +'%')
                window.Refresh()
                penalty_weights[w_agg] = []
                SolExists = solve_instance(E_v, start, Alt, w_agg, file_paths, file_indic)
                if not SolExists:
                    break
                ind +=1
            if SolExists:
                file_weights.write('Weights' + "\t".join( ['p'+str(i) for i in range(1,nb_paths+1)]))
                for w_agg in w_aggs:
                    file_weights.write(str(w_agg[0]) +","+str(w_agg[1]) + '\t')
                    line = [str(w_agg[0]) +","+str(w_agg[1])]
                    print('nb_paths',nb_paths, penalty_weights)
                    for p in range(nb_paths):
                        if str(p) in penalty_weights[w_agg]:
                            line.append('1')
                            file_weights.write('\t1')
                        else:
                            file_weights.write('\t0')
                            line.append('0')
                    TABLE_WEIGHTS.append(line)
                    file_weights.write('\n')
                file_weights.close()
                file_paths.close()
                file_indic.close()
                PATH_GENERATED = True
                MAX_COL = max([len(a) for a in TABLE_PATHS])
                print(MAX_COL)
                for i in range(len(TABLE_PATHS)):
                    if MAX_COL-len(TABLE_PATHS[i]) > 0:
                        for j in range(MAX_COL-len(TABLE_PATHS[i])):
                            TABLE_PATHS[i].insert(len(TABLE_PATHS[i])-3, '')
                    TABLE_PATHS[i].insert(0,'p '+ str(i+1))
                print(TABLE_PATHS)
                headers_paths = ['Path'] + list(['Step ' + str(a) for a in range(1,MAX_COL-3+1)])+['bottleneck','OP. changes', 'Ranked earned']
                print(headers_paths)
                TABLE_WEIGHTS = sorted(TABLE_WEIGHTS, key=lambda x: x[1:])
                headers_weights = ['Weights'] + list([ 'p '+ str(i+1) for i in range(len(TABLE_PATHS))])
                print(headers_weights)
                window.close()
                visualization()

        elif event == '_tablepath_':
            selected_row = values['_tablepath_'][0]
            print(selected_row)
            window.FindElement('_selectedpath_').Update(value = 'p'+ str(int(selected_row)+1))
            window.Refresh()
        elif event == '_vizpath_':
            print(values['_tablepath_'])
            if values['_tablepath_'] == []:
                sg.popup_quick_message('Please select a path in the list!', auto_close=True, non_blocking=True, font='Default 18')
            else:
                path, refs, poids = viz_path[int(values['_tablepath_'][0])]
                centroid = compute_centroide(poids)
                percentage = round(len(poids)/len(w_aggs),2)
                fig = pathplot.get_figure(path, refs, G[1:], str(centroid)+' % ='+str(percentage))
                #window_plot(fig)
        elif event == '-Draw-':
            ind = values['-Draw_ind-']
            if ind.isdigit():
                fig = weights_zone(ind.strip())
                fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, fig, fig_canvas_agg)
                window.Refresh()
            else:
                sg.popup_quick_message('Please provide the path number that must be analyzed and drawn', auto_close=True, non_blocking=True, font='Default 18')








###---------------- ---------------------------- --------------------------------------
###---------------- INITIALISATION DU PROGRAMME  --------------------------------------
###---------------- ---------------------------- --------------------------------------


#---- Find the file containing the MAVT model represented as partial values for each criterion for each alternative
sg.tk.scrolledtext.ScrolledText = ScrolledText
sg.theme('Reddit')
MIN_RANK_INCREASE = 1
PATH_FILES = ''
fig_canvas_agg = ''
MCDM = ''
problem = MCDMProblem()
#Fast Test ULB parameters
E_v, Alt,G = [], [], [] 
DM_name = ''
DEGRAD_CRIT = []
IMPROV_CRIT = []
#problem.qs = [0, 0, 0, 0, 0, 0]
#problem.ps = [5, 10, 20, 10, 10, 3]
#problem.vs = [20, 20, 25, 20, 30, 10]
#problem.ws = [0.1, 0.2, 0.2, 0.2, 0.2, 0.1]
#problem.midpoints= [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [3.0, 10.0, 5.0, 10.0, 25.0, 10.0], [10.0, 30.0, 10.0, 20.0, 45.0, 20.0], [18.0, 50.0, 25.0, 30.0, 60.0, 30.0], [30.0, 75.0, 45.0, 40.0, 70.0, 40.0]]
#problem.midpoints=[[0]*6, [35]*6, [50]*6, [65]*6, [100]*6]
#problem.DEGRAD_CRIT = [18.8, 25.7, 0, 0, 0, 0]
#problem.IMPROV_CRIT = [20, 27.5, 100, 100, 100, 100]
problem.ws = []
problem.midpoints= []
problem.DEGRAD_CRIT = []
problem.IMPROV_CRIT = []
DEGRAD_CRIT = []
IMPROV_CRIT = []
MINIMUMS, MAXIMUMS = [], []
browse_path = ''
m, n ,start = 0, 0, 0
V0, V1, V2, V3, V4, V5 = True, True, False, False, False, False
values_displayed, headers_paths, headers_weights= [], [], []
w_aggs= [(j/10, round(i/10 - j/10,2)) for i in range(2,10) for j in range(1, i)]
PATH_GENERATED, transformed, discretized= False, False, True
DM_name = ''
TABLE_PATHS = []
TABLE_WEIGHTS = []
paths_evaluated = []
viz_path = []
penalty_weights = {}
nb_paths = 0
visualization()

