import numpy as np
import time
def Preference(A,B,p,q):
    res=0
    if A-B>q:
        if A-B>p:
            res=1
        else:
            res=(A-B-q)/(p-q)
    return res

def Pi(F,p,q,w):
    n=len(F)
    k=len(F[0])
    res=[]
    for i in range(n):
        x=[]
        for j in range(n):
            item=0
            for l in range(k):
                item=item+w[l]*Preference(F[i][l],F[j][l],p[l],q[l])
            x.append(item)
        res.append(x)
    return res
def pos_flow(Pi,i):
    n=len(Pi)
    return sum(Pi[i])*(1/(n-1))

def neg_flow(Pi, i):
    n=len(Pi)
    return sum([e[i] for e in Pi])*(1/(n-1))

def seuils(F,lp,lq):
    n=len(F)
    k=len(F[0])
    p=[]
    q=[]
    for l in range(k):
        x=[]
        for i in range(n-1):
            for j in range(i+1,n):
                x.append(abs(F[i][l]-F[j][l]))
        p.append(np.percentile(np.array(x), lp))
        q.append(np.percentile(np.array(x), lq))
    return (p,q)
def net_flow(F,p,q,w):
    n=len(F)
    k=len(F[0])
    Pij=Pi(F,p,q,w)
    res=[]
    for i in range(n):
        item=0
        for j in range(n):
            item=item+Pij[i][j]-Pij[j][i]
        res.append((item/(n-1),i))
    return res
        
            




        
        
            
                
            
        



        
    
