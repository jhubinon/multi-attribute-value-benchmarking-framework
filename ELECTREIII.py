####################################### ELECTRE ##########################################
import numpy as np
#------ Outranking relations building ------#
def conc_crit(a,b,q,p):
    if a + q >= b:
        return 1
    elif a + p < b:
        return 0
    else:
        return (a - b + p)/(p-q)
def conc(a,b,qs,ps,ws):
    return sum([conc_crit(a[k], b[k], qs[k], ps[k])*ws[k] for k in range(len(a))])/sum(ws)

def disc_crit(a,b,p,v):
    if b <= a + p:
        return 0
    elif b > a + v:
        return 1
    else:
        return (b - a - p)/(v - p)

def credibility(a,b,q,p,w,v):
    C = conc(a,b,q,p,w)
    D = [disc_crit(a[k], b[k], p[k], v[k]) for k in range(len(a))]
    if max(D) <= C:
        return C
    else:
        return C*np.prod([ (1-D[k])/(1-C) for k in range(len(a)) if D[k] > C])

def credibility_matrix(E,q,p,w,v):
    mat = [[0]*len(E) for i in range(len(E))]
    max_val = 0
    for i, a in enumerate(E):
        for j, b in enumerate(E):
            mat[i][j] = credibility(a,b,q,p,w,v)
            if mat[i][j] > max_val:
                max_val = mat[i][j]
    return mat

#------ Outranking relations exploiting ------#

def disc_thres(max_val, alpha = 0.3, beta = 0.15):
    return max_val*alpha + beta

def outranks(Sab, Sba, l):
    if Sab > l and Sab - Sba > disc_thres(Sab):
        return 1
    else:
        return 0 

def strength(i,cred_mat,level,used):
    return sum([1 for j in used if outranks(cred_mat[i][j],cred_mat[j][i],level)])

def weakness(i,cred_mat,level,used):
    return sum([ 1 for j in used if outranks(cred_mat[j][i],cred_mat[i][j], level)])

def qualification(i, cred_mat, level, used):
    return strength(i, cred_mat, level, used) - weakness(i, cred_mat, level, used)

def qualif_mat(cred_mat, level, used):
    mat = []
    for i in range(len(cred_mat)):
        if i in used:
            mat.append(qualification(i,cred_mat,level, used))
        else:
            mat.append(-1000)
    return mat

def reduce_mat(m,indexes):
    ind = sorted(indexes)[::-1]
    for i in ind :
        del m[i]
    l = len(m)
    for i in range(l):            
        for j in ind:
            del m[i][j]




def distillation_down(cred_mat):
    R = {}
    A = set(range(len(cred_mat)))
    cut_level = np.max(np.array(cred_mat))
    iter = 1
    while len(A) > 1:
        cut_level = cut_level - disc_thres(cut_level)
        qm = qualif_mat(cred_mat, cut_level, A)
        max_q = max(qm)
        #print(max_q, qm)
        D = {i for i in range(len(qm)) if i in A and qm[i] == max_q}
        #print(D)
        cut_level_init = cut_level
        while len(D) > 1 and cut_level > 0:
            cut_level = cut_level - disc_thres(cut_level)
            qm = qualif_mat(cred_mat, cut_level, D)
            max_q = max(qm)
            D = {i for i in range(len(qm)) if i in D and qm[i] == max_q}
        for i in D:
            R[i] = iter
        A = A.difference(D)
        #print(A,'after')
        iter +=1
        #print(iter,R)
        cut_level = cut_level_init
    if A != set():
        for elem in A:
            R[elem] = iter
    return R

def distillation_upward(cred_mat):
    R = {}
    A = set(range(len(cred_mat)))
    cut_level = np.max(np.array(cred_mat))
    iter = 1
    while len(A) > 1:
        cut_level = cut_level - disc_thres(cut_level)
        qm = qualif_mat(cred_mat, cut_level, A)
        qm = [-qm[i] if qm[i] != -1000 else qm[i] for i in range(len(qm)) ]
        max_q = max(qm)
        #print(max_q, qm)
        D = {i for i in range(len(qm)) if i in A and qm[i] == max_q}
        #print(D)
        cut_level_init = cut_level
        while len(D) > 1 and cut_level > 0:
            cut_level = cut_level - disc_thres(cut_level)
            qm = qualif_mat(cred_mat, cut_level, D)
            qm = [-qm[i] if qm[i] != -1000 else qm[i] for i in range(len(qm)) ]
            max_q = max(qm)
            D = {i for i in range(len(qm)) if i in D and qm[i] == max_q}
        for i in D:
            R[i] = iter
        A = A.difference(D)
        #print(A,'after')
        iter +=1
        #print(iter,R)
        cut_level = cut_level_init
    if A != set():
        for elem in A:
            R[elem] = iter

    return R


### --------------------------- Final outranking relation -----------

def final_outranking(R1, R2, nb_alt):
    mat = [[0]*nb_alt for i in range(nb_alt)]
    for i in range(nb_alt):
        for j in range(i+1, nb_alt):
            if (R1[i] < R1[j] and R2[i] > R2[j]) or (R1[i] == R1[j] and R2[i] > R2[j]) or (R1[i] < R1[j] and R2[i] == R2[j]):
                mat[i][j] = 1
            elif R1[i] == R1[j] and R2[i] == R2[j]:
                mat[i][j] = 1
                mat[j][i] = 1
    return mat

def median_preorder(R1, R2, nb_alt):
    max_R2 = max(R2.values())
    mat = [[0]*nb_alt for i in range(nb_alt)]
    rang = []
    for i in range(nb_alt):
        rang.append((R1[i]+(max_R2 - R2[i] +1)/ 2, i))
        for j in range(i+1, nb_alt):
            if R1[i]+(max_R2 - R2[i] +1)/ 2 < R1[j]+(max_R2 - R2[j] +1)/ 2:
                mat[i][j] = 1
            elif R1[i]+(max_R2 - R2[i] +1)/ 2 == R1[j]+(max_R2 - R2[j] +1)/ 2:
                mat[i][j] = 1
                mat[j][i] = 1
            elif R1[i]+(max_R2 - R2[i] +1)/ 2 > R1[j]+(max_R2 - R2[j] +1)/ 2:
                mat[j][i] = 1
    rang = sorted(rang)
    rang = [e[1] for e in rang]
    return mat, rang


