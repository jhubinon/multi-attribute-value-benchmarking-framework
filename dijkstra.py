from collections import defaultdict

class Graph:
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.weights = {}
        self.cons = {}
        self.observed = {}
        self.V = 0
        self.arcs = []
        self.index_nodes = {}
        self.N = 0
    def add_node(self, value):
        if value not in self.nodes:
            self.N += 1
        self.nodes.add(value)
        
    def consist(self,from_node, to_node,c):
        self.cons[(from_node, to_node)] = c

    def add_edge(self, from_node, to_node, distance, obs):
        if from_node in self.edges:
            if to_node not in self.edges[from_node]:
                self.edges[from_node].append(to_node)
                self.weights[(from_node, to_node)] = distance
                self.V += 1
                self.arcs.append([from_node, to_node, distance])
            
        else:
            self.arcs.append([from_node, to_node, distance])
            self.edges[from_node].append(to_node)
            self.V += 1
            self.weights[(from_node, to_node)] = distance
        self.observed[(from_node, to_node)] = obs
class Node:
    def __init__(self, perf, obs=[None]):
        self.end_node = False
        self.performances = perf
        self.observed = obs[0]
        self.prec = set()
        self.follow = set()
        #self.aggreg = 0
    def add_prec(self,node):
        self.prec.add(tuple(node.performances))
    def add_follow(self,node):
        self.follow.add(tuple(node.performances))
    def new_prec(self, node):
        self.prec = set()
        self.prec.add(tuple(node.performances))
def simple_dijkstra(adjList,source,sink = None):
    n = len(adjList) 
    visited = [False]*n
    distance = [float('inf')]*n
    prev = [None] * n

    distance[source] = 0
    while True: 
        current_weight, min_node = min([(distance[i],i) for i in range(n) if distance[i] <= 0  and not visited[i]]+[(float('inf'), None)])
        if not min_node:
            break
        visited[min_node] = True

        #nodes.remove(min_node)

        for neighbor,cost in adjList[min_node]:
            weight = current_weight + cost
            if weight < distance[neighbor]:
                distance[neighbor] = weight
                prev[neighbor] = [min_node]
                visited[neighbor] = False
            elif weight == distance[neighbor]:
                prev[neighbor].append(min_node)

    return distance, prev


